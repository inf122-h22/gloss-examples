A few examples using gloss. Run an example using `cabal run ex<i>`, where `<i>` is
a number from 1 to 4. The escape key closes the game. For the non-game examples
you can zoom/pan the picture, see
[the documentation](https://hackage.haskell.org/package/gloss-1.13.2.2/docs/Graphics-Gloss.html)
for controls.

If you want to make changes to the examples, use `cabal repl ex<i>` which will
launch `ghci` and load example number `<i>`, and then you can run the game by calling `main` in
the ghci terminal. Whenever you make any changes to the source code
(e.g. `app/Ex1.hs`), reload them with the `:reload` command (`:r` for short) in
ghci, and call `main` again. This should result in much faster edit/test
iterations compared to recompiling with `cabal run`.

# Ex4

To run `ex4` you first need to put your `Oblig0Common.hs` in the `app` directory.

![Ex4 screenshot](img/oblig0.png)

# Note for OS X users

To run on OS X, you will have to delete the file `cabal.project` first.

# Project structure

Usually a cabal project only needs a project-name.cabal file, and source files,
so don't be confused by all the files in this folder;

* app: Contains the source code files.
* gloss-examples.cabal: Contains project configuration.
* cabal.project: Contains project configuration. Used here to tell cabal to use a custom version of the gloss library.
* libfreeglut.dll: Needed to use Gloss under Windows. Provided here as a convenience.
* flake.lock / flake.nix: The author (me) uses these to run the project under NixOS. Can safely be ignored.
