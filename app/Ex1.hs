module Main where

import Graphics.Gloss

main :: IO ()
main = display window white picture

picture :: Picture
picture = pictures $ map createSquareAtAngle [ 0, pi / 10 .. 2 * pi ]
    where createSquareAtAngle r = let x = cos r * distance
                                      y = sin r * distance
                                      distance = 100
                                      width = 10
                                      height = 10
                                   in translate x y $ rectangleSolid width height

window :: Display
window = InWindow "Nice Window" (768, 768) (10, 10)

