module Main where

import Graphics.Gloss
import Oblig0Common

main :: IO ()
main = do
    input <- readFile "testData0"
    let datapoints = map read (lines input) :: [(Double, Double, Double)]
    let summedData = map (\(a, b, c) -> a + b + c) datapoints
    let dataLength = length summedData
    let processedData =
            reverse $ take dataLength $
                applyFilter (hpf highPassCutoff) $
                    applyFilter (lpf lowPassCutoff) $
                        reverse summedData

    display window white (picture summedData processedData)

picture :: [Double] -> [Double] -> Picture
picture d1 d2 = pictures [ color red $ makeLine d1
                         , color blue $ makeLine d2
                         , line $ [(0, 0), (500, 0)] -- visualize the x axis
                         ]
                             where
                                 -- Our data is a list of y-coordinates for
                                 -- our plot, but we also need to specify the
                                 -- x-coordinates when drawing our plot as a
                                 -- sequence of line segments.
                                 --
                                 -- The assignment text says that the data
                                 -- represents a 100 Hz signal, so we can say
                                 -- that the x-coordinates are implicitly 0.01s
                                 -- apart. Here we add in those coordinates;
                                 -- the first data point gets x-coordinate 0.0,
                                 -- the second gets x-coordinate 0.1, then 0.02,
                                 -- etc.
                                 --
                                 -- Finally the `line` function from Gloss turns
                                 -- the list of (x, y) points into a chain of
                                 -- line segments which can be displayed on
                                 -- the screen.
                                 makeLine :: [Double] -> Picture
                                 makeLine xs = line $ zip [0, 0.01 ..] $ map realToFrac xs

window :: Display
window = InWindow "Nice Window" (768, 768) (10, 10)

