module Main where

import Graphics.Gloss

main :: IO ()
main = animate window white drawFrame

drawFrame :: Float -> Picture
drawFrame time = pictures $ map createSquare [(x, y) | x <- [-12..12], y <- [-12..12] ]
    where createSquare (x, y) = move x y $ color (getColor x y) $ rectangleSolid 20 20
          move x y = translate (x * 30) (y * 30)
          getColor x y = if abs (sin ((x/5) + time*4) - (y / 10)) <= 0.2
                                 then red
                                 else greyN 0.3

window :: Display
window = InWindow "Nice Window" (768, 768) (10, 10)

